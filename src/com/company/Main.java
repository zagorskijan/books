package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        Book books[] = new Book[4];

        for (int i = 0; i < books.length; i++) {

            Book london = new Book();
            london.author = "Jack London";
            london.name = "Martin Eden";
            london.year = 1909;
            books[0] = london;

            Book dickens = new Book();
            dickens.author = "Charles Dickens";
            dickens.name = "David Copperfield";
            dickens.year = 1849;
            books[1] = dickens;

            Book remarque = new Book();
            remarque.author = "Erich Maria Remarque";
            remarque.name = "Flotsam";
            remarque.year = 1939;
            books[2] = remarque;

            Book poe = new Book();
            poe.author = "Edgar Allan Poe";
            poe.name = "Morella";
            poe.year = 1838;
            books[3] = poe;
        }
    }
}